package jUnit_GeneradorIp;

import java.util.Random;

public class GeneradorIp {
	
	Random rnd = new Random();
	public int generarNumero(int min, int max) {
		int cantidadNumeros = max - min;
		return rnd.nextInt(cantidadNumeros)+min;
	}
	
	public String generarIp() {
		int primer = generarNumero(0,255);
		int ultimo = generarNumero(0,255);
		if (primer == 0 && ultimo == 0) {
			 primer = generarNumero(0,255);
			 ultimo = generarNumero(0,255);
		}
		return primer + "." + generarNumero(0,255) + "."+ generarNumero(0,255) + "."+ ultimo; 
	}
}
