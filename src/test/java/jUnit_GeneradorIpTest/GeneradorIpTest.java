package jUnit_GeneradorIpTest;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import jUnit_GeneradorIp.GeneradorIp;

class GeneradorIpTest {
	GeneradorIp generador;
	@BeforeEach
	void setUp() throws Exception {
	 generador = new GeneradorIp();
		
	}

	@Test
	void testGenerarNumero() {
		for (int i = 0; i < 100; i++) {
			int numeroGenerado = generador.generarNumero(0, 254);
			assertTrue(numeroGenerado >= 0 && numeroGenerado <=254);
		}
	}
	
	@Test
	void testGenerarIp() {
		String ip = generador.generarIp();
		String[] ip1 = ip.split("\\.");
		assertTrue(Integer.parseInt(ip1[0]) != 0 && Integer.parseInt(ip1[3]) != 0);
		
	}

}
